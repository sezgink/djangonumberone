from django.shortcuts import render, get_object_or_404, redirect
from .models import Product
from .models import Customer
from .forms import ProductForm
from .forms import CustomerForm
from django.views import generic
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy


class Startpage(generic.ListView):
    template_name = 'startpage.html'

    def get_queryset(self):
        return Product.objects.all()
    # def get_queryset(self):
    #     return Customer.objects.order_by('name'[:5])


class ProductListView(generic.ListView):
    template_name = 'product_list.html'
    context_object_name = 'product_list'

    def get_queryset(self):
        return Product.objects.all()


class CustomerListView(generic.ListView):
    template_name = 'customer_list.html'
    context_object_name = 'customer_list'

    def get_queryset(self):
        return Customer.objects.all()


class ProductDetailView(generic.DetailView):
    model = Product
    template_name = 'product_detail.html'


class CustomerDetailView(generic.DetailView):
    model = Customer
    template_name = 'customer_detail.html'


class ProductCreateView(generic.CreateView):
    model = Product
    template_name = 'product_create.html'
    form_class = ProductForm
    success_url = reverse_lazy('ProductListView')


class CustomerCreateView(generic.CreateView):
    model = Customer
    template_name = 'customer_create.html'
    form_class = CustomerForm
    success_url = reverse_lazy('CustomerListView')


class ProductUpdateView(generic.UpdateView):
    model = Product
    form_class = ProductForm
    # fields = ['title', 'price' ]
    template_name = 'product_create.html'
    success_url = reverse_lazy('Startpage')
    #template_name_suffix = '_update_form'


class CustomerUpdateView(generic.UpdateView):
    model = Customer
    form_class = CustomerForm
    template_name = 'customer_create.html'
    success_url = reverse_lazy('Startpage')
    #template_name_suffix = '_update_form'


class ProductDeleteView(DeleteView):
    model = Product
    success_url = reverse_lazy('ProductListView')
    template_name = 'product_delete.html'


class CustomerDeleteView(DeleteView):
    model = Customer
    success_url = reverse_lazy('CustomerListView')
    template_name = 'customer_delete.html'


def product_delete_all_view(request):
    obj = Product.objects.all()
    # confirming delete
    if request.method == "POST":
        obj.delete()
        return redirect('Startpage')
        # return redirect(views.startpage)
    return render(request, 'product_delete_all.html', {'del': obj})


def customer_delete_all_view(request):
    obj = Customer.objects.all()
    # confirming delete
    if request.method == "POST":
        obj.delete()
        return redirect('Startpage')
        # return redirect(views.startpage)
    return render(request, 'product_delete_all.html', {'del': obj})

    # def startpage(request):
    #     product = Product.objects.all()
    #     customer = Customer.objects.all()
    #     return render(request, 'startpage.html', {'product': product, 'customer' : customer})

    # def product_detail_view(request, product_id):
    #     # anstatt try/catch kann man folgenden kürzeren code verwenden, um
    #     # bei einem Fehler einen Fehlercode bzw. eine Fehlererklärung
    #     # anzuzeigen. Folgendes passiert hier:
    #     # Prüfung ob objekt existier, wenn es nicht existiert, dann
    #     # gebe den 404 Fehlercode aus.
    #     obj = get_object_or_404(Product, id=product_id)
    #     return render(request, 'product_detail.html', {'product': obj})
    #
    # def customer_detail_view(request, customer_id):
    #     obj = get_object_or_404(Customer,id=customer_id)
    #     return render(request, 'customer_detail.html', {'customer': obj})

    # def product_create_view(request):
    #     # success_url = '//'
    #     form = ProductForm(request.POST or None)
    #     if form.is_valid():
    #         form.save()
    #         return redirect('Startpage')
    #     #form = ProductForm()
    #
    #     return render(request, 'product_create.html', {'form': form})

    # def CustomerCreateView(request):
    #     # success_url = '//'
    #     form = CustomerForm(request.POST or None)
    #     if form.is_valid():
    #         form.save()
    #         return redirect('Startpage')
    #     #form = ProductForm()
    #
    #     return render(request, 'customer_create.html', {'formc': form})

    # def product_delete_view(request, id):
    #     obj = get_object_or_404(Product, id=id)
    #     # confirming delete
    #     if request.method == "POST":
    #         obj.delete()
    #         return redirect('Startpage')
    #     return render(request, 'product_delete.html', {'del': obj})
