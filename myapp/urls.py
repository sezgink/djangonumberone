"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from . import views

urlpatterns = [
    path('', views.Startpage.as_view(template_name='startpage.html'), name='Startpage'),

    path('product/', views.ProductListView.as_view(), name='ProductListView'),
    path('customer/', views.CustomerListView.as_view(), name='CustomerListView'),

    path('product/<int:pk>/',views.ProductDetailView.as_view(), name='ProductDetailView'),
    path('customer/<int:pk>/',views.CustomerDetailView.as_view(), name='CustomerDetailView'),

    path('createp/',views.ProductCreateView.as_view(), name='ProductCreateView'),
    path('createc/',views.CustomerCreateView.as_view(), name='CustomerCreateView'),

    path('updatep/<int:pk>/', views.ProductUpdateView.as_view(), name='ProductUpdateView'),
    path('updatec/<int:pk>/', views.CustomerUpdateView.as_view(), name='CustomerUpdateView'),


    path('product/<int:pk>/delete/',views.ProductDeleteView.as_view(), name='ProductDeleteView'),
    path('customer/<int:pk>/delete/',views.CustomerDeleteView.as_view(), name='CustomerDeleteView'),
    path('product/deleteall/',views.product_delete_all_view, name='product_delete_all_view'),
    path('customer/deleteall/',views.customer_delete_all_view, name='customer_delete_all_view'),

]
